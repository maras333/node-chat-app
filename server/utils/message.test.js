const expect = require('expect');
const {generateMessage, generateLocationMessage} = require('./message');

describe('generateMessage', () => {
  it('should generate a correct message object', () => {
    let from = 'Admin';
    let text = "Test message";
    let message = generateMessage(from, text);
    expect(message).toBeTruthy();
    expect(typeof message.createdAt).toBe('number');
    expect(message).toMatchObject({from, text});
  });
});

describe('generateLocationMessage', () => {
  it('should generate a correct location  message object', () => {
    let from = 'Admin';
    let latitude = 33;
    let longitude = 33;
    var url = `https://www.google.com/maps?q=33,33`;
    let message = generateLocationMessage(from, latitude, longitude);
    expect(message).toBeTruthy();
    expect(typeof message.createdAt).toBe('number');
    expect(message).toMatchObject({from, url});
  });
});
