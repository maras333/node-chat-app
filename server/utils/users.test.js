const expect = require('expect');

const {Users}= require('./users');

describe('Users', () => {

  beforeEach(() => {
    users = new Users();

    users.users = [{
      id: '1',
      name: 'John',
      room: 'Room A'
    },{
      id: '2',
      name: 'Jena',
      room: 'Room B'
    },{
      id: '3',
      name: 'George',
      room: 'Room A'
    }];
  })

  it('should add new user', () => {
    let users = new Users();
    let user = {
      id: '121',
      name: 'Maras',
      room: 'Room A'
    }
    let resUser = users.addUser(user.id, user.name, user.room);

  });

  it('should remove a user', () => {
    let user = users.removeUser('2');
    expect(users.users.length).toBe(2);
    expect(user).toEqual({id: '2', name: 'Jena', room: 'Room B'});
  });

  it('should not remove a user', () => {
    let user = users.removeUser(4);
    expect(user).toBe(undefined);
    expect(users.users.length).toBe(3);
  });

  it('should find user', () => {
    let userId = '2';
    let user = users.getUser(userId);
    expect(user.id).toBe(userId);
  });

  it('should not find a user', () => {
    let userId = '100';
    let user = users.getUser(userId);
    expect(user).toBe(undefined);
  });

  it('should return names for Room A', () => {
    let resUserList = users.getUserList('Room A');

    expect(resUserList).toEqual(['John', 'George']);
  });

  it('should return names for Room B', () => {
    let resUserList = users.getUserList('Room B');

    expect(resUserList).toEqual(['Jena']);
  });
});
