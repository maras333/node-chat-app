const expect = require('expect');
const {isValidString} = require('./validation');

describe('validation', () => {
  it('should accept valid strings', () => {
    let name = 'John';
    let room = 'Room1';
    let message = isValidString(name);
    expect(message).toBe(true);
  });

  it('should reject just spaces', () => {
    let name = '';
    let message = isValidString(name);
    expect(message).toBe(false);
  });

  it('should reject non strings', () => {
    let name = 111;
    let message = isValidString(name);
    expect(message).toBe(false);
  });
});
